//
//  EditWaypointViewController.swift
//  iFound
//
//  Created by Alex on 28/10/2018.
//  Copyright © 2018 Alex Saraev. All rights reserved.
//

import UIKit
import MobileCoreServices

class EditWaypointViewController: UIViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Model
    var waypointToEdit: EditableWaypoint? 
    
    private func updateUI() {
        nameTextField?.text = waypointToEdit?.name
        infoTextField?.text = waypointToEdit?.info
        updateImage()
    }
    
    // MARK: - Outlets
    @IBOutlet weak var nameTextField: UITextField! { didSet {nameTextField.delegate = self }}
    @IBOutlet weak var infoTextField: UITextField! { didSet {infoTextField.delegate = self }}
    
    //MARK: - LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.becomeFirstResponder()
        preferredContentSize = view.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        updateUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidLoad()
        listenToTextFields()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopListeningToTextFields()
    }
    
    private var ntfObserver: NSObjectProtocol?
    private var itfObserver: NSObjectProtocol?
    
    private func listenToTextFields() {
        let center = NotificationCenter.default
        let queue = OperationQueue.main
        
        ntfObserver = center.addObserver(forName: UITextField.textDidChangeNotification, object: nameTextField, queue: queue) { notification in
            if let waypoint = self.waypointToEdit {
                waypoint.name = self.nameTextField.text
            }
        }
        
        itfObserver = center.addObserver(forName: UITextField.textDidChangeNotification, object: infoTextField, queue: queue) { notification in
            if let waypoint = self.waypointToEdit {
                waypoint.info = self.infoTextField.text
            }
        }
    }
    
    private func stopListeningToTextFields() {
        if let observer = ntfObserver {
            NotificationCenter.default.removeObserver(observer)
        }
        if let observer = itfObserver {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    //MARK: - Image
    var imageView = UIImageView()
    
    @IBOutlet weak var stackView: UIStackView!
        @IBOutlet weak var imageViewContainer: UIView! {
            didSet {
                imageViewContainer.addSubview(imageView)
            }
        }
    
    @IBAction func takePhoto() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.mediaTypes = [kUTTypeImage as String]
            picker.delegate = self
            picker.allowsEditing = true
            present(picker, animated: true, completion: nil)
            
        }
    }
    
    //MARK: - UIImagePickerControllerDelegate
    private func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        var image = info[UIImagePickerController.InfoKey.editedImage.rawValue] as? UIImage // UIImagePickerControllerEditedImage
        
        if image == nil {
            image = info[UIImagePickerController.InfoKey.originalImage.rawValue] as? UIImage
        }
        imageView.image = image
        makeRoomForImage()
        saveImageInWaypoint()
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    func saveImageInWaypoint() {
        if let image = imageView.image {
            if let imageData = image.jpegData(compressionQuality: 1) { //UIImageJPEGRepresentation(image, 1.0)
                let fileManager = FileManager()
                if let docsDir = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
                    let unique = Date.timeIntervalSinceReferenceDate
                    let url = docsDir.appendingPathComponent("\(unique).jpg")
                    let path = url.absoluteString
                    if (try? imageData.write(to: url)) != nil {
                        waypointToEdit?.links = [GPX.Link(href: path)]
                    }
                }
            }
        }
    }
    //MARK: - UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension EditWaypointViewController {
    
    func updateImage() {
        if let url = waypointToEdit?.imageURL {
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                if let imageData = try? Data(contentsOf: url) {
                    if url == self?.waypointToEdit?.imageURL {
                        if let image = UIImage(data: imageData) {
                            DispatchQueue.main.async {
                                self?.imageView.image = image
                                self?.makeRoomForImage()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func makeRoomForImage() {
        var extraHeight: CGFloat = 0
        if let aspectRation = imageView.image?.aspectRation, aspectRation > 0 {
            if let width = imageView.superview?.frame.size.width {
                let height = width / aspectRation
                imageView.frame = CGRect(x: 0, y: 0, width: width, height: height)
                extraHeight = imageView.frame.height + (stackView.frame.origin.y * 2)
            }
        } else {
            imageView.frame = CGRect.zero
            extraHeight = imageView.frame.height
        }
        print("----SIZE----", preferredContentSize.height)
        preferredContentSize = CGSize(width: preferredContentSize.width, height: preferredContentSize.height + extraHeight)
    }
}
