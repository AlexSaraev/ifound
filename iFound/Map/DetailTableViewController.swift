//
//  DetailTableViewC.swift
//  iFound
//
//  Created by Alex on 28/10/2018.
//  Copyright © 2018 Alex Saraev. All rights reserved.
//

import UIKit

class DetailTableViewController: UITableViewController {
    
    var waypoint: GPX.Waypoint?

    @IBOutlet weak var storeTitle: UILabel!
    @IBOutlet weak var price: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        switch waypoint!.latitude {
        case 55.81890:
            storeTitle.text = "\"re.Store\""
            price.text = "72 990 ₽"
        case 55.818371:
            storeTitle.text = "\"МВидео\""
            price.text = "71 490 ₽"
        case 55.820272:
            storeTitle.text = "\"DNS\""
            price.text = "70 790 ₽"
        default: break
        }
    }


}
