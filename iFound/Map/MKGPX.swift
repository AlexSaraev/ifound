//
//  MKGPX.swift
//  iFound
//
//  Created by Alex on 28/10/2018.
//  Copyright © 2018 Alex Saraev. All rights reserved.
//

import Foundation
import MapKit


extension UIViewController {
    var contentViewController: UIViewController {
        if let navcon = self as? UINavigationController {
            return navcon.visibleViewController ?? navcon
        } else {
            return self
        }
    }
    
}

class EditableWaypoint: GPX.Waypoint {
    override var coordinate: CLLocationCoordinate2D {
        get {
            return super.coordinate
        }
        set {
            latitude = newValue.latitude
            longitude = newValue.longitude
        }
    }
    
    override var thumbnailURL: URL? { return imageURL }
    override var imageURL: URL? { return links.first?.url }
}

extension UIImage {
    var aspectRation: CGFloat {
        return size.height != 0 ? size.width / size.height : 0
    }
}
