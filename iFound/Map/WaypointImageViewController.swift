//
//  WaypointImageViewController.swift
//  iFound
//
//  Created by Alex on 28/10/2018.
//  Copyright © 2018 Alex Saraev. All rights reserved.
//

import UIKit

class WaypointImageViewController: ImageViewController {
    
    var waypoint: GPX.Waypoint? {
        didSet {
            imageURL = waypoint?.imageURL
            title = waypoint?.name
            updateEmbeddedMap()
        }
    }
    private var smvc: SimpleMapViewController?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Embed Map" {
            smvc = segue.destination as? SimpleMapViewController
            updateEmbeddedMap()
        }
        
        if let detailTVC = segue.destination as? DetailTableViewController {
            detailTVC.waypoint = waypoint
        }
        
    }
    
    private func updateEmbeddedMap() {
        if let mapView = smvc?.mapView, let waypoint = waypoint {
            mapView.mapType = .hybrid
            mapView.removeAnnotations(mapView.annotations)
            mapView.addAnnotation(waypoint)
            mapView.showAnnotations(mapView.annotations, animated: true)
        }
    }

}
